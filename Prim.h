#ifndef PRIM_H
#define PRIM_H

#include <iostream>
#include <cstring>
using namespace std;


class Prim {
    private:
     
    public:
        /* constructor*/
        Prim();

        void correr_programa(int n, int opc);
        void leer_nodos (char *vector, int n);
        void inicializar_L (int **L, int n);
        void inicializar_vector_caracter (char *vector, int n);
        int prim(int **M,int n,int **L,int** matriz_modifc);
        void aplicar_prim ( int **L, int **M, int n,int **matriz_modif);
        int calcular_minimo(int dw, int dv, int mvw);
        void imprimir_vector_entero(int **vector, int n);
        void imprimir_matriz(int **matriz,int n);
        void imprimir_grafo_arbol(int **matriz, char *vector, int n);
        void imprimir_grafo_arbol_cost_min(int **matriz, char *vector, int n);
        void inicializar_matriz_enteros (int **matriz, int n);  
        void actualizar_matriz_cost_min (int **matriz,int n,int x,int y, int arista);      
        
};
#endif
