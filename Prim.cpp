#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include "Prim.h"
using namespace std;
#define INF 9999999

Prim::Prim()
{
}
// Cada vez que se corre esta funcion se crea una nueva matriz tamaño n*n
void Prim::correr_programa(int n, int opc)
{
 
  char V[n], U[n], VU[n];
  int **D;
  int i = 0;
  int o = 0;
  int origen;
  int **matriz;
  int **matriz_modif;
  matriz = new int*[n];
 
  for(int i=0; i<n; i++)
        matriz[i] = new int[n];

    inicializar_matriz_enteros(matriz, n);
  // inicializar matriz L[]  
  for(int i=0; i<n; i++)
        D[i] = new int[n];

    inicializar_L(D,n);

  for(int i=0; i<n; i++)
        matriz_modif[i] = new int[n];

    inicializar_L(matriz_modif,n);


  inicializar_vector_caracter(V, n);

  if (opc == 1)
  { 
    leer_nodos(V, n);
    aplicar_prim(D, matriz, n, matriz_modif);
    imprimir_grafo_arbol(matriz, V,n);
    imprimir_grafo_arbol_cost_min(matriz_modif, V,n);
  }
}

// copia contenido inicial a D[] desde la matriz M[][].
void Prim::inicializar_L(int **L, int n)
{
  int col, fila;

  for (col = 0; col < n - 1; col++)
  {
    for (fila = 0; fila < 2; fila++)
    {
      L[fila][col] = 0;
    }
  }
}

// inicializa con espacios el arreglo de caracteres.
void Prim::inicializar_vector_caracter(char *vector, int n)
{
  int col;

  for (col = 0; col < n; col++)
  {
    vector[col] = ' ';
  }
}

void Prim::aplicar_prim(int **L, int **M, int n, int **matriz_modif)
{
  int i;
  int x, y, arista;

  // estado inicial

  cout << "---------Estados iniciales ---------------------------------------\n";
  imprimir_matriz(M, n);
  cout << endl;

  imprimir_vector_entero(L, n);
  cout << "------------------------------------------------------------------\n\n";

  int v_y = prim(M, n, L, matriz_modif);
  cout << "\nLa nueva matriz de costo minimo:" << endl;
  imprimir_matriz(matriz_modif, n);
}

//Aqui se encuentran los costos minimos
int Prim::prim(int **M, int n, int **L, int **matriz_modif)
{
  int no_edge, l; 
  int selected[n];
  memset(selected, false, sizeof(selected));

  no_edge = 0;
  l = 0;
  selected[0] = true;

  int x; //  row number
  int y; //  col number


  cout << "Aristas"
       << " : ";
  cout << endl;
  while (no_edge < n - 1)
  {


    int min = INF;
    x = 0;
    y = 0;

    for (int i = 0; i < n; i++)
    {
      if (selected[i])
      {
        for (int j = 0; j < n; j++)
        {
          if (!selected[j] && M[i][j])
          { 
            if (min >= M[i][j])
            {
              min = M[i][j];
              x = i;
              y = j;
            }
          }
        }
      }
    }
    cout << "L[" << l << "]: (" << x << "-" << y << ")";
    cout << endl;
    selected[y] = true;
    no_edge++;
    l++;
    actualizar_matriz_cost_min(matriz_modif, n, x, y, min);
  }

  return y;
}

void Prim::inicializar_matriz_enteros(int **matriz, int n)
{
  int col;
  int num = 0;
  for (int fila = 0; fila < n; fila++)
  {
    for (col = num; col < n; col++)
    {
      int costo;

      if (fila == col)
      {
        costo = 0;
        matriz[fila][col] = costo;
      }
      else
      {
        cout << "Ingrese la distancia entre "
             << "[" << fila << "," << col << "] =" << endl;
        cin >> costo;
        matriz[fila][col] = costo;
        matriz[col][fila] = costo;
      }
    }
    num = num + 1;
  }
}

//Se modifica la matriz para que contenga los costos minimos 
void Prim::actualizar_matriz_cost_min(int **matriz, int n, int x, int y, int costo)
{
  int col, fila;
  int num = 0;

  for (fila = 0; fila < n; fila++)
  {
    for (col = num; col < n; col++)
    {

      if (fila == x && col == y)
      {
        matriz[fila][col] = costo;
        matriz[col][fila] = costo;
      }
    }
  }
}


// lee datos de los nodos.
// inicializa utilizando código ASCII.
void Prim::leer_nodos(char *vector, int n)
{
  int i;
  int inicio = 97;

  for (i = 0; i < n; i++)
  {
    vector[i] = inicio + i;
  }
}

// Imprime las aristas del la matriz L
void Prim::imprimir_vector_entero(int **vector, int n)
{
  int i, o, x;

  for (i = 0; i < n - 1; i++)
  {
    cout << "L[" << i << "]: (";
    for (o = 0; o < 2; o++)
    {
      printf("%c", vector[o][i]);
      if (o == 0)
      {

        cout << "-";
      }
    }
    cout << ")" << endl;
  }

  cout << "\n";
}

// imprime el contenido de una matriz bidimensional de enteros.
void Prim::imprimir_matriz(int **matriz, int n)
{
  int i, j;
  cout << endl;
  for (i = 0; i < n; i++)
  {
    for (j = 0; j < n; j++)
    {
      cout << "matriz[" << i << "," << j << "]: " << matriz[i][j] << " ";
    }
    cout << "\n";
  }
}

// genera y muestra apartir de una matriz bidimensional de enteros
// el grafo correspondiente.
void Prim::imprimir_grafo_arbol(int **matriz, char *vector, int n)
{
  int i, j;
  int num = 0;
  ofstream fp;

  fp.open("grafo.txt");

  fp << "graph G {" << endl;
  fp << "graph [rankdir=LR]" << endl;
  fp << "node [style=filled fillcolor=olivedrab1];" << endl;

  for (i = 0; i < n; i++)
  {
    for (j = num; j < n; j++)
    {
      // evalua la diagonal principal.
      if (i != j)
      {
        if (matriz[i][j] > 0)
        {
          fp << vector[i] << " -- " << vector[j] << " [label=" << matriz[i][j] << "];" << endl;
        }
      }
    }
    num = num + 1;
  }

  fp << "}" << endl;
  fp.close();

  system("dot -Tpng -ografo.png grafo.txt");
  system("eog grafo.png &");
}
// grafo de la nueva matriz con costo minimo
void Prim::imprimir_grafo_arbol_cost_min(int **matriz, char *vector, int n)
{
  int i, j;
  int num = 0;
  ofstream fp;

  fp.open("grafo_cost_min.txt");

  fp << "graph G {" << endl;
  fp << "graph [rankdir=LR]" << endl;
  fp << "node [style=filled fillcolor=olivedrab1];" << endl;

  for (i = 0; i < n; i++)
  {
    for (j = num; j < n; j++)
    {
      // evalua la diagonal principal.
      if (i != j)
      {
        if (matriz[i][j] > 0)
        {
          fp << vector[i] << " -- " << vector[j] << " [label=" << matriz[i][j] << "];" << endl;
        }
      }
    }
    num = num + 1;
  }

  fp << "}" << endl;
  fp.close();

  system("dot -Tpng -ografo_cost_min.png grafo_cost_min.txt");
  system("eog grafo_cost_min.png &");
}
