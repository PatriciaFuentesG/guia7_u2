#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "Prim.h"
using namespace std;


int menu(){
  int opc;
  cout<<"\n\n";
  cout <<"**********Menu***********"<<endl;
  cout <<"Aplicar Prim    ______[1]"<<endl;
  cout <<"Salir_________________[0]"<<endl;
  cout <<"*************************"<<endl;
  cout <<"Seleccione: ";
  cin >> opc;
  cout << endl;
  
 
 return opc;
}

int main(int argc, char **argv) {
 int opc = -1;
 if (atoi(argv[1])<=1){
        cout << "Uso: \n./matriz n" << endl;
        return -1;
  }else{
  Prim *digrafica = new Prim;

  while(opc != 0){
   opc = menu();
   if(opc ==1){
      digrafica -> correr_programa(atoi(argv[1]),opc);
      
   }else{
     return -1;
   }
 }

  delete digrafica;}

  return 0;
}

